<?php

return [
    'Id'          => 'ID',
    'Name'        => '用户名',
    'Phone'       => '手机号',
    'Openid'      => '微信用户识别',
    'Nickname'    => '微信昵称',
    'Status'      => '状态',
    'Sex'         => '性别',
    'Createtime'  => '创建时间',
    'Updatetime'  => '更新时间',
    'Nospeak'     => '禁言',
    'Nopublish'   => '能发布论坛',
    'Loginnumber' => '登录次数',
    'Status 0'      => '禁用',
    'Status 1'      => '正常',
    'Sex 1'         => '男',
    'Sex 0'         => '女',
    'Nospeak 0'     => '禁言',
    'Nospeak 1'     => '正常',
    'Nopublish 1'   => '是',
    'Nopublish 0'   => '否',

];

<?php

return [
    'Id'         => 'id',
    'Member_id'  => '会员id',
    'Status'     => '状态',
    'Images'     => '图片路径',
    'Nocomment'  => '是否可以评论',
    'Nocomment 0'  => '否',
    'Nocomment 1'  => '是',
    'Content'    => '发布内容',
    'Createtime' => '发布时间',
    'Updatetime' => '更新时间',
    'Forum_id'   => '关联论坛文章id'
];

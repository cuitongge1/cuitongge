<?php

/**
 * 微信处理类
 */
namespace think\exception;
use think\Exception;

define("SYS_TIME",time());
define("WX_PATH",'./Application/api/');
class Weixin
{

	private $wxcfg;
	public function __construct() {

		$this->wxcfg = $this->lod_cfg('_cfg');
        $this->macid = $this->wxcfg['pay']['payid'];
	}

	/*

	 读取config文件

	*/

	public function lod_cfg($name) {
		$name = "wx".$name;
		if(file_exists(WX_PATH.'Conf/'.$name.'.php')){
			$config = require WX_PATH.'Conf/'.$name.'.php';
		}
		return $config;
	}

	/*

	读取主号cfg文件

	*/
	public function bak_cfg($key){
		//Appsecret , AppId
		$cfg = $this->lod_cfg('_cfg');
		return $cfg['cfg'][$key];
	}
	public function fucfg($wxid,$key){
		//Appsecret , AppId
		$cfg = $this->lod_cfg($wxid.'_cfg');
		return $cfg[$key];
	}

	public function add_oauth($wxid,$openid,$gzsta=0,$userinfo=array()){
		if(!$openid){
			return false;
		}
		$uinfo = null; //用户信息查找 todo
		if(!$uinfo){
			// 没有注册时，就直接注册会员账号
			if($userinfo){
				$wxdata = $userinfo;
			}else{
				$wxdata = $this->get_wxinfo($wxid,$openid);
			}
			$uinfo = $this->get_mywxinfo($wxdata);
			$uinfo['uid'] = 0 ;
			$uinfo['wxyid'] = $wxid ;
			$uinfo['gzsta'] = $gzsta ;
			//用户信息添加 todo
		}
		return $uinfo;
	}



	/*

	注册会员（自动注册）
	传入分组ID ，返回 uid,name,username,groupid,nickname

	*/
	public function adduser($openid,$gid=3,$gzsta=0,$userinfo=array()){

		if(!$openid){
			return false;
		}
		$where['openid'] = "$openid";
		$uinfo = M('member_oauth')->where($where)->find();
		//如果没有注册过
		if(empty($uinfo)) {
			$wx_info = $this->get_wxinfo('',$openid);
			if(!$wx_info){
				return false;
			}
			M()->startTrans();
			//  在用户表中添加数据
			$member = M('member');
			$data['username'] = 'wx_'.time()."_".rand(1000,9999);
			$data['name'] = $wx_info['nickname'];
			$data['sex'] = $wx_info['sex'];
			$data['img_head'] = $wx_info['headimgurl'];
			$member_add = $member->add($data);

			// 在member_oauth 中添加数据
			$data_oauth['uid'] = $member_add;
			$data_oauth['openid'] = $wx_info['openid'];
			$data_oauth['hytime'] = time();
			$data_oauth['sta'] = 1;
			$data_oauth['nickname'] =$wx_info['nickname'] ;
			$data_oauth['oauth'] = "weixin";
			$data_oauth['sex'] = $wx_info['sex'];
			$data_oauth['expire_at'] = SYS_TIME;
			$data_oauth['avatar'] = $wx_info['headimgurl'];
			$member_auth_add = M('member_oauth')->add($data_oauth);
			if($member_add && $member_auth_add){
				// 添加成功
				M()->commit();
				return array_merge($data,$data_oauth);
			}else{
				M()->rollback();
				return false;
			}

		}

		$member_info = M("member")->where('id='.$uinfo['uid'])->find();
		//error_log(json_encode($member_info),3,"member_add.txt");
		return  array_merge($uinfo,$member_info);


	}





	/**/
	public function wx_exuser($user,$ex,$strarr=''){
		$exdata = $this->lod_cfg('wx_exuser');
		$ex_arr = $exdata[$ex];
		if(is_array($ex_arr) && $ex_arr['sta']==1){

			//获取事件状态
			$ex_sta = $this->wx_onldata($user['uid'], $ex , array('str'=>$strarr));

			if($ex_sta){
				//优惠券赠送
				if($ex_arr['vol']){
					$this->wx_addvol( $user, array('lai'=>$ex_arr['tit'],'beizhu'=>'消费时选择使用') , $ex_arr['vol']);
					$this->log_txt(WX_PATH."/wx_exuser.txt",'优惠券赠送---'.json_encode($ex_arr));
				}

				//赠送积分
				if($ex_arr['jifen']){
					$this->member_model->update_score(0, $user['uid'], $ex_arr['jifen'] , $ex , $ex_arr['tit']."赠送积分");
					$this->log_txt(WX_PATH."/wx_exuser.txt",'赠送积分--'.json_encode($ex_arr));
				}

			}else{
				$this->log_txt(WX_PATH."/wx_exuser.txt",'1551--'.$ex_sta.'-'.json_encode($ex_arr));
			}
		}

	}



	/*

	用户唯一执行过程记录

	*/
	public function wx_onldata($uid,$type,$strarr=array()){
		if(!$uid || !$type){
			return false;
		}
		$onldata =null; //用户信息查找 todo：
		if($onldata){
			$onl = dr_string2array($onldata['onldata']);
			if(!$onl[$type]){
				$onl[$type]['sta']=1;
				$onl[$type]['time']=SYS_TIME;
				$onl[$type]['str']=$strarr['str'];
				//用户信息更新todo
				return true;
			}
		}else{
			$onl=array();
			$onl[$type]['sta']=1;
			$onl[$type]['time']=SYS_TIME;
			$onl[$type]['str']=$strarr['str'];
			$updata['uid'] = $uid;
			$updata['complete'] = 0;
			$updata['onldata'] = dr_array2string($onl);
			//用户信息插入 todo
			return true;
		}
		return false;
	}



	/***********************************************************/
	//推荐用户相关方法
	/***********************************************************/

	//自动注册用户
	public function login($tid=0,$red_url='',$appid='',$scope='snsapi_userinfo'){
		if(strstr(dr_now_url(),'?')){
			$burl = urlencode(dr_now_url().'&wxtid='.$tid.'');
		}else{
			$burl = urlencode(dr_now_url().'?wxtid='.$tid.''); //dr_authcode(dr_now_url().'&tid='.$tid.'', 'ENCODE',$appid);
		}
		set_cookie('WX_TID', $burl , 1800);
		set_cookie('WX_BAKURL', $burl , 1800);
		if(!$red_url){
			$red_url = SITE_URL.'weixin/?c=oauth2';
			$appid = $this->dweixin->bak_cfg('AppId');
		}
		header("Location: https://open.weixin.qq.com/connect/oauth2/authorize?appid=". $appid ."&redirect_uri=". urlencode($red_url) ."&response_type=code&scope=".$scope."&state=test#wechat_redirect");
		exit;
	}

	//获取共享地址的
	//自动注册用户
	public function login_addr($tid=0,$red_url='',$appid='',$scope='snsapi_userinfo'){
		$burl = urlencode(dr_now_url());
		set_cookie('WX_BAKURL', $burl , 1800);
		if(!$red_url){
			$red_url = SITE_URL.'weixin/?c=oauth2';
			$appid = $this->dweixin->bak_cfg('AppId');
		}
		header("Location: https://open.weixin.qq.com/connect/oauth2/authorize?appid=". $appid ."&redirect_uri=". urlencode($red_url) ."&response_type=code&scope=".$scope."&state=test#wechat_redirect");
		exit;
	}

	//判断地址acctoken过去，获取
	public function getaddr_token($uid){

		$data =null; //用户授权信息查找

		if($data['addr_data']){
			$json_arr = json_decode($data['addr_data'],true);
			//echo print_r($json_arr);exit;
			if(time()-$json_arr["time"]>=7200){

				return false;
			}else{
				return $json_arr['access_token'];
			}
		}
		return false;
	}

	//ADDr 签名
	public function getaddr_Sign($uid){
		$acc_token = $this->getaddr_token($uid);
		if(!$acc_token){
			$this->login_addr(TID,'http://l2.sx-ug.com/?m=fweixin&c=oauth2&a=wai_addr','wx33a536d08562c292','snsapi_base');
		}
		$wxdata = $this->wxcfg['cfg'];
		$jsApiObj["appid"] = $wxdata['AppID'];
		$jsApiObj["url"] = dr_now_url();
		$jsApiObj["nonceStr"] = $this->createNoncestr();
		$jsApiObj["accessToken"] = $acc_token;
		$jsApiObj["timeStamp"] = strval(SYS_TIME);
		$jsApiObj["addrSign"] = $this->getSign_sha1($jsApiObj);
		//$arameters = json_encode($jsApiObj);
		return $jsApiObj;
	}


	/***************************************************************/

	/*

	共享TOKEN

	*/
	public function USER_TOKEN(){

		$data = $this->lod_cfg('cache_acctoken');
		if(SYS_TIME-$data["time"]>=$data['expires_in']){
			$data = $this->get_acctoken();
		}
		return $data;
	}
	/*

	共享验证

	*/
	public function USER_TOKEN_CK($ckstr){
		$cfg = $this->wxcfg['cfg'];
		$str = md5($cfg["AppID"].'JFF'.$cfg["Appsecret"]);
		if($ckstr==$str){
			return $this->USER_TOKEN();
		}
		return FALSE;
	}
	/*

	判断access_token是否失效

	*/
	public function red_acctoke($wxid){
		$data = $this->lod_cfg($wxid.'_acc');
 // error_log($acc_token,3,"heloo.txt");
		if(SYS_TIME-$data["time"]>=$data['expires_in'] || empty($data["access_token"])){
			$data = $this->get_acctoken($wxid);
		}

		return $data["access_token"];
	}

	/*

	获取access_token

	*/

	public function get_acctoken($wxid){
		$cfg = $this->lod_cfg($wxid.'_cfg');
		$cfg= $cfg['cfg'];
	//	error_log("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$cfg["AppID"]."&secret=".$cfg["Appsecret"],3,"hello.txt");
		$xmlinfo = $this->get_url("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$cfg["AppID"]."&secret=".$cfg["Appsecret"]);
//error_log($xmlinfo,3,"hello23.txt");
		$xmlinfo2 = json_decode($xmlinfo,true);
		$xmlinfo2["time"]= time();

		if($xmlinfo2["errcode"]){
		//error_log(json_encode(xmlinfo2),3,"hello23.txt");
		}else{
			//error_log(json_encode($xmlinfo2),3,"hello2.txt");
			// 写入文件中 存储
			// 更新微信jsapi_ticket 配置信息
			$path = WX_PATH.'Conf/'.'wx_acc'.CONF_EXT;
			file_put_contents($path, "<?php \nreturn " . stripslashes(var_export($xmlinfo2, true)) . ";");
		}
		return $xmlinfo2;
	}



	/*

	获取用户基本信息（UnionID机制）

	*/
	public function get_wxinfo($wxid,$openid){
		$acctoken = $this->red_acctoke($wxid);
		$json = $this->get_url("https://api.weixin.qq.com/cgi-bin/user/info?access_token=". $acctoken ."&openid=". $openid ."&lang=zh_CN");
		$json_data = json_decode($json,true);
		if($json_data["errcode"]){
			return '';
		}
		return $json_data;
	}

	/*

	生成带参数二维码

	*/
	public function get_wxma($kid,$type=0){

		$acctoken = $this->red_acctoke();
		$poststr='{"action_name": "QR_LIMIT_SCENE", "action_info": {"scene": {"scene_id":'.$kid.'}}}';
		if($type){
			$poststr='{"action_name": "QR_LIMIT_STR_SCENE", "action_info": {"scene": {"scene_str":"'.$kid.'"}}}';
		}
		$json = $this->post_url("https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=". $acctoken ."",$poststr);
		$json_data = json_decode($json,true);

		if($json_data["errcode"]){
			$this->log_txt(WX_PATH."/dweixin-err.txt",'生成二维码失败token---'.$acctoken.' --------type:'.$type.'--kid:'.$kid.' '.$poststr.'- '.$json);
			return false;
		}
		return $json_data;
	}

	/*

	获取多客服列表

	*/
	public function get_kefulist(){

		$acctoken = $this->red_acctoke();
		$json = $this->get_url("https://api.weixin.qq.com/cgi-bin/customservice/getonlinekflist?access_token=". $acctoken ."");
		$json_data = json_decode($json,true);

		if($json_data["errcode"]){
			$this->log_txt(WX_PATH."/dweixin-errlog.txt",'获取多客服列表失败'.$json);
			return '';
		}
		return $json_data;
	}

	/*

	建立客服会话

	*/
	public function get_kefuok($kf_account,$openid,$text='后台转接用户'){
		$data = array(
			'kf_account'=>$kf_account,
			'openid'=>$openid,
			'text'=>urlencode($text),
		);
		$acctoken = $this->red_acctoke();
		$json = $this->post_url('https://api.weixin.qq.com/customservice/kfsession/create?access_token='. $acctoken .'' , urldecode(json_encode($data)));
		$json_data = json_decode($json,true);
		//$this->log_txt(WX_PATH."/dweixin-errlog.txt",'建立客服会话'.$json);
		if($json_data["errcode"]){
			if($json_data["errcode"]=='61458'){
				$ykf = str_replace("customer accepted by ", "", $json_data["errmsg"]);
				$this->get_kefuclose($ykf,$openid);
			}
			$this->log_txt(WX_PATH."/dweixin-errlog.txt",'建立客服会话失败--'.$json.'---'.$ykf);
			return '';
		}
		return $json_data;
	}
	/*

	关闭会话

	*/
	public function get_kefuclose($kf_account,$openid,$text='后台结束会话'){
		$data = array(
			'kf_account'=>$kf_account,
			'openid'=>$openid,
			'text'=>urlencode($text),
		);
		$acctoken = $this->red_acctoke();
		$json = $this->post_url('https://api.weixin.qq.com/customservice/kfsession/close?access_token='. $acctoken .'' , urldecode(json_encode($data)));
		$json_data = json_decode($json,true);

		if($json_data["errcode"]){
			$this->log_txt(WX_PATH."/dweixin-errlog.txt",'关闭会话失败--'.$json);
			return '';
		}
	}

	//获取JS接口信息
	public function getSignPackage($wxid='') {

		$jsapiTicket = $this->getJsApiTicket();
		$url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$timestamp = strval(time());
		$nonceStr = $this->createNonceStr();
		$cfg = $this->lod_cfg($wxid.'_cfg');

		// 这里参数的顺序要按照 key 值 ASCII 码升序排序
		$string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";

		$signature = sha1($string);

		$signPackage = array(
			"appid"     => $cfg['cfg']['AppID'],
			"nonceStr"  => $nonceStr,
			"timestamp" => $timestamp,
			"url"       => $url,
			"signature" => $signature,
			"rawString" => $string
		);
		return $signPackage;
	}

	private function createNonceStr($length = 16) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$str = "";
		for ($i = 0; $i < $length; $i++) {
			$str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
		}
		return $str;
	}

	private function getJsApiTicket($wxid) {
		// jsapi_ticket 应该全局存储与更新，以下代码以写入到文件中做示例
		$filename =WX_PATH."/". $wxid.'_jstick';
		$data = $this->lod_cfg($filename); //加载微信jsapi_ticket 配置信息

		if (!$data || $data['expire_time'] < time()){
			$accessToken = $this->red_acctoke($wxid);

			// 如果是企业号用以下 URL 获取 ticket
			// $url = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=$accessToken";
			$url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";
			$res = json_decode($this->get_url($url),true);

			$ticket = $res['ticket'];
			if ($ticket) {
				$cdata['expire_time'] = time() + 6990;
				$cdata['jsapi_ticket'] = $ticket;
				// 更新微信jsapi_ticket 配置信息
				file_put_contents($filename.CONF_EXT, "<?php \nreturn " . stripslashes(var_export($cdata, true)) . ";", LOCK_EX);

			}
		} else {
			$ticket = $data['jsapi_ticket'];
		}

		return $ticket;
	}

	//js发起
	public function getParameters($wxid,$prepay_id){
		$wxdata = $this->lod_cfg($wxid.'_cfg');
		//$pydata = $this->wxcfg['pay'];
		$jsApiObj["appId"] = $wxdata['cfg']['AppID'];
		$jsApiObj["timeStamp"] = strval(SYS_TIME);
		$jsApiObj["nonceStr"] = $this->createNoncestr();
		$jsApiObj["package"] = "prepay_id=$prepay_id";
		$jsApiObj["signType"] = "MD5";
		$jsApiObj["paySign"] = $this->getSign($jsApiObj,$wxdata['pay']['key']);
		$arameters = json_encode($jsApiObj);
		return $arameters;
	}

	//统一下单
	public function getjspay($wxid=''){
		$response = $this->postXml($wxid);
		$result = $this->xmlToArray($response);
		$prepay_id = $result["prepay_id"];

		return $this->getParameters($wxid,$prepay_id);
	}
	/**
	 * 	作用：设置请求参数
	 */
	public function setParameter($parameter, $parameterValue){
		$this->parameters[$this->trimString($parameter)] = $this->trimString($parameterValue);
	}
	public function postXml($wxid){
		$xml = $this->createXml($wxid);

		$this->log_txt(WX_PATH."/jstick-errlog.txt",$xml);
		$response = $this->post_url("https://api.mch.weixin.qq.com/pay/unifiedorder",$xml);
		$this->log_txt(WX_PATH."/jstick-errlog1.txt",json_encode($response));
		return $response;
	}


	public function createXml($wxid){
		$cfg = $this->lod_cfg($wxid.'_cfg');

		if($this->parameters["out_trade_no"] == null) {
			throw new Exception("缺少统一支付接口必填参数out_trade_no！"."<br>");
		}elseif($this->parameters["body"] == null){
			throw new Exception("缺少统一支付接口必填参数body！"."<br>");
		}elseif ($this->parameters["total_fee"] == null ) {
			throw new Exception("缺少统一支付接口必填参数total_fee！"."<br>");
		}elseif ($this->parameters["notify_url"] == null) {
			throw new Exception("缺少统一支付接口必填参数notify_url！"."<br>");
		}elseif ($this->parameters["trade_type"] == "JSAPI" &&
			$this->parameters["openid"] == NULL){
			throw new Exception("统一支付接口中，缺少必填参数openid！trade_type为JSAPI时，openid为必填参数！"."<br>");
		}

		$this->parameters["trade_type"] = 'JSAPI';
		$this->parameters["appid"] = $cfg['cfg']['AppID'];
		$this->parameters["mch_id"] = $cfg['pay']['payid'];
		$this->parameters["spbill_create_ip"] = $_SERVER['REMOTE_ADDR'];
		$this->parameters["nonce_str"] = $this->createNonceStr();
		$this->parameters["sign"] = $this->getSign($this->parameters,$cfg['pay']['key']);

		return  $this->arrayToXml($this->parameters);
	}
//********************************************************************扫码支付****************************************************//
    //统一下单
    public function get_app_data_xml($wxid=''){
        $response = $this->postXml_native($wxid);
        $result = $this->xmlToArray($response);
        return $result;
    }
    public function getjspay_native($wxid=''){
        $response = $this->postXml_native($wxid);
        $result = $this->xmlToArray($response);
        $code_url = $result["code_url"];

        return $code_url;
    }
    public function postXml_native($wxid){
        $xml = $this->createXml_native($wxid);

        $this->log_txt(WX_PATH."/jstick-errlog.txt",$xml);
        $response = $this->post_url("https://api.mch.weixin.qq.com/pay/unifiedorder",$xml);
        $this->log_txt(WX_PATH."/jstick-errlog1.txt",json_encode($response));
        return $response;
    }

    public function createXml_native($wxid){
        $cfg = $this->lod_cfg($wxid.'_cfg');

        if($this->parameters["out_trade_no"] == null) {
            throw new Exception("缺少统一支付接口必填参数out_trade_no！"."<br>");
        }elseif($this->parameters["body"] == null){
            throw new Exception("缺少统一支付接口必填参数body！"."<br>");
        }elseif ($this->parameters["total_fee"] == null ) {
            throw new Exception("缺少统一支付接口必填参数total_fee！"."<br>");
        }elseif ($this->parameters["notify_url"] == null) {
            throw new Exception("缺少统一支付接口必填参数notify_url！"."<br>");
        }elseif ($this->parameters["trade_type"] == "JSAPI" &&
            $this->parameters["openid"] == NULL){
            throw new Exception("统一支付接口中，缺少必填参数openid！trade_type为JSAPI时，openid为必填参数！"."<br>");
        }

        $this->parameters["trade_type"] = 'NATIVE';
        $this->parameters["appid"] = $cfg['cfg']['AppID'];
        $this->parameters["mch_id"] = $cfg['pay']['payid'];
        $this->parameters["spbill_create_ip"] = $_SERVER['REMOTE_ADDR'];
        $this->parameters["nonce_str"] = $this->createNonceStr();
        $this->parameters["sign"] = $this->getSign($this->parameters,$cfg['pay']['key']);

        return  $this->arrayToXml($this->parameters);
    }



	public function getorder($oid){
		$xml = $this->getorderxml($oid);
		$response = $this->post_url("https://api.mch.weixin.qq.com/pay/orderquery",$xml);

		return $response;
	}
	/**
	 * 查询订单生成接口参数xml
	 */
	public function getorderxml($oid){
		$wxdata = $this->wxcfg['cfg'];
		$pydata = $this->wxcfg['pay'];
		try
		{
			$this->parameters["out_trade_no"] = $oid;//公众账号ID
			$this->parameters["appid"] = $wxdata['AppID'];//公众账号ID
			$this->parameters["mch_id"] = $pydata['payid'];//商户号
			$this->parameters["nonce_str"] = $this->createNonceStr();//随机字符串
			$this->parameters["sign"] = $this->getSign($this->parameters,$pydata['key']);//签名
			return  $this->arrayToXml($this->parameters);
		}catch (SDKRuntimeException $e)
		{
			die($e->errorMessage());
		}
	}

	/*

	客服接口-发消息

	*/
	public function sendmsg($openid,$type='text',$data){
		$acctoken = $this->red_acctoke();
		$_data = array();
		$_data['touser']=''.$openid;
		$_data['msgtype']=$type;
		$_data[$type] = $data;

		$url="https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=".$acctoken."";
		$json = $this->post_url($url , urldecode(json_encode($_data)));
		$jdata = json_decode($json,true);
		if($json['errcode']==0){
			return true;
		}
		$this->log_txt(WX_PATH."/dweixin-err.txt",'客服接口-发消息 -'.$json['errmsg'].' '.$json);
		return FALSE;

	}

	/*
	模版消息-发消息
	*/
	public function  sendMmsg($data){
		$acctoken = $this->red_acctoke();
		$json = $this->post_url('https://api.weixin.qq.com/cgi-bin/message/template/send?access_token='.$acctoken , urldecode(json_encode($data)));
		$jdata = json_decode($json,true);
		$this->log_txt(WX_PATH."/wx-log.txt",'模版消息-发消息 -'.urldecode(json_encode($data)).' '.$json);
		if($jdata['errcode']){
			$this->log_txt(WX_PATH."/dweixin-err.txt",'模版消息发送失败： -'.urldecode(json_encode($data)).' '.$json);
		}
	}

	/*

	生成被动回复xml
	无消息回复 success

	*/
	public function huifu_xml($Username,$Uopenid,$type,$val){
		//加载文本模版
		$textTpl = "<xml>
			<ToUserName><![CDATA[%s]]></ToUserName>
			<FromUserName><![CDATA[%s]]></FromUserName>
			<CreateTime>%s</CreateTime>
			<MsgType><![CDATA[%s]]></MsgType>
			<Content><![CDATA[%s]]></Content>
			</xml>";
		//加载图文模版
		$picTpl = "<xml>
			 <ToUserName><![CDATA[%s]]></ToUserName>
			 <FromUserName><![CDATA[%s]]></FromUserName>
			 <CreateTime>%s</CreateTime>
			 <MsgType><![CDATA[%s]]></MsgType>
			 <ArticleCount>%s</ArticleCount>
			 <Articles>%s</Articles>
			 <FuncFlag>1</FuncFlag>
			 </xml> ";
		$FromUserName=$Username;
		$ToUserName= $Uopenid;
		$CreateTime=time();
		if($type=="text"){
			$MsgType ="text" ;
			$Content = $val;
			$resultStr = sprintf($textTpl, $ToUserName, $FromUserName, $CreateTime, $MsgType, $Content);
			//$this->log_txt(WX_PATH."/wx-log.txt",'11--'.$resultStr);
		}else if($type=="news"&& $val){
			$MsgType ="news" ;
			$ArticleCount=count($val);
			$Articles="";
			$ArticlesTpl="<item>
						 <Title><![CDATA[%s]]></Title>
						 <Description><![CDATA[%s]]></Description>
						 <PicUrl><![CDATA[%s]]></PicUrl>
						 <Url><![CDATA[%s]]></Url>
						 </item>";
			foreach($val as $value){
				$Title = $value['title'];
				$Description=$value['description'];
				$PicUrl= $value['thumb'];
				$Url=$value['url'];
				$Articles = $Articles.''.sprintf($ArticlesTpl,$Title,$Description,$PicUrl,$Url);
			}
			$resultStr = sprintf($picTpl, $ToUserName, $FromUserName, $CreateTime, $MsgType, $ArticleCount, $Articles);
		}
		//$this->log_txt(WX_PATH."/wx-log.txt",'22'.$resultStr);
		return $resultStr;
	}
	//作用：生成签名
	public function getSign_sha1($Obj){

		foreach ($Obj as $k => $v){
			$Parameters[$k] = $v;
		}
		//签名步骤一：按字典序排序参数
		ksort($Parameters);
		$String = $this->formatBizQueryParaMap($Parameters, false);
		$String = sha1($String);
		//$result_ = strtoupper($String);
		return $String;
	}

	//作用：生成签名
	public function getSign($Obj,$key){

		foreach ($Obj as $k => $v){
			$Parameters[$k] = $v;
		}
		//签名步骤一：按字典序排序参数
		ksort($Parameters);
		$String = $this->formatBizQueryParaMap($Parameters, false);
		//echo '【string1】'.$String.'</br>';
		//签名步骤二：在string后加入KEY

		$String = $String."&key=".$key;
		//echo "【string2】".$String."</br>";
		//签名步骤三：MD5加密
		$String = md5($String);
		//echo "【string3】 ".$String."</br>";
		//签名步骤四：所有字符转为大写
		$result_ = strtoupper($String);
		//echo "【result】 ".$result_."</br>";
		return $result_;
	}

	//作用：格式化参数，签名过程需要使用
	public function formatBizQueryParaMap($paraMap, $urlencode){
		$buff = "";
		ksort($paraMap);
		foreach ($paraMap as $k => $v){
			if($urlencode){
				$v = urlencode($v);
			}
			//$buff .= strtolower($k) . "=" . $v . "&";
			$buff .= $k . "=" . $v . "&";
		}

		if (strlen($buff) > 0){
			$reqPar = substr($buff, 0, strlen($buff)-1);
		}
		return $reqPar;
	}

	public function trimString($value){
		$ret = null;
		if (null != $value)
		{
			$ret = $value;
			if (strlen($ret) == 0)
			{
				$ret = null;
			}
		}
		return $ret;
	}
	//作用：将xml转为array
	public function xmlToArray($xml){
        //将XML转为array
        //禁止引用外部xml实体
       // libxml_disable_entity_loader(true);
		//将XML转为array
		$array_data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
		return $array_data;
	}
	//作用：array转xml
	public function arrayToXml($arr){
		$xml = "<xml>";
		foreach ($arr as $key=>$val){
			if (is_numeric($val)){
				$xml.="<".$key.">".$val."</".$key.">";
			}else
				$xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
		}
		$xml.="</xml>";
		return $xml;
	}
//扫码支付模式一
    public function  getpayurl($orderid){

        $cfg = $this->lod_cfg('_cfg');
        if($this->parameters["product_id"] == null) {
            throw new Exception("缺少统一支付接口必填参数product_id！" . "<br>");
        }
        $this->parameters["appid"] = $cfg['cfg']['AppID'];
        $this->parameters["mch_id"] = $cfg['pay']['payid'];
        $this->parameters["time_stamp"] = time();
        $this->parameters["nonce_str"] = $this->createNonceStr();
        $this->parameters["sign"] = $this->getSign($this->parameters,$cfg['pay']['key']);

        return  $this->formatBizQueryParaMap($this->parameters);
    }

    /**
     * 退款
     */
    public function refund_back($transaction_id,$total_fee,$refund_fee){

        $this->parmwx['transaction_id'] = $transaction_id ;
        $this->parmwx['total_fee'] = $total_fee;
        $this->parmwx['refund_fee'] = $refund_fee;
        $this->parmwx['out_refund_no'] =$this->macid.rand(100,999).date("YmdHis");
        $this->parmwx['op_user_id']  = $this->macid;//换成买家id
        $cfg = $this->lod_cfg('_cfg');
        $this->parmwx['appid'] = $cfg['cfg']['AppID'];//公众账号ID
        $this->parmwx['mch_id'] = $this->macid;//商户号
        $this->parmwx['nonce_str'] = $this->createNonceStr();

        if(!$this->parmwx['out_trade_no']&& !$this->parmwx['transaction_id']) {
            throw new \Think\Exception("退款申请接口中，out_trade_no、transaction_id至少填一个！");
        }else if(!$this->parmwx['out_refund_no']){
            throw new \Think\Exception("退款申请接口中，缺少必填参数out_refund_no！");
        }else if(!$this->parmwx['total_fee']){
            throw new \Think\Exception("退款申请接口中，缺少必填参数total_fee！");
        }else if(!$this->parmwx['refund_fee']){
            throw new \Think\Exception("退款申请接口中，缺少必填参数refund_fee！");
        }else if(!$this->parmwx['op_user_id']){
            throw new \Think\Exception("退款申请接口中，缺少必填参数op_user_id！");
        }
        $this->parmwx["sign"] = $this->getSign($this->parmwx,$cfg['pay']['key']);
        $xml =  $this->arrayToXml( $this->parmwx);
        $url = "https://api.mch.weixin.qq.com/secapi/pay/refund";
        $data_r= $this->xmlToArray($this->postXmlCurl($xml,$url,true,30));
         $res = $this->reportCostTime($data_r);
        return $res;
    }

    private function reportCostTime($data){
        //如果仅失败上报
        if(array_key_exists("return_code", $data) &&
            $data["return_code"] == "SUCCESS" &&
            array_key_exists("result_code", $data) &&
            $data["result_code"] == "SUCCESS") {
            return  1;
        }else if($data["return_code"] == "FAIL" ){
            return $data["return_msg"];
        }else if($data["return_code"] == "SUCCESS" &&
            array_key_exists("result_code", $data) &&
            $data["result_code"] == "FAIL"){
            return $data;
        }
    }
    /**
     *
     * 参数数组转换为url参数
     * @param array $urlObj
     */
    private function ToUrlParams($urlObj)
    {
        $buff = "";
        foreach ($urlObj as $k => $v)
        {
            $buff .= $k . "=" . $v . "&";
        }

        $buff = trim($buff, "&");
        return $buff;
    }
	/*

	 POST

	*/
	public function post_url($url,$postdate)
	{
		//初始化curl
		$ch = curl_init();
		//设置超时
		curl_setopt($ch, CURLOPT_TIMEOUT,30);
		//这里设置代理，如果有的话
		//curl_setopt($ch,CURLOPT_PROXY, '8.8.8.8');
		//curl_setopt($ch,CURLOPT_PROXYPORT, 8080);
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
		curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,FALSE);
		//设置header
		//curl_setopt($ch, CURLOPT_HEADER, array("Content-type:application/json;charset=UTF-8"));
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		//要求结果为字符串且输出到屏幕上
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		//post提交方式
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postdate);
		//运行curl
		$data = curl_exec($ch);
		//返回结果
		if($data){
			curl_close($ch);
			return $data;
		}else{
			$error = curl_errno($ch);
			curl_close($ch);
			return "curl出错，错误码:$error"."<br>";
			return false;
		}
	}
    /**
     * 以post方式提交xml到对应的接口url
     *
     * @param string $xml  需要post的xml数据
     * @param string $url  url
     * @param bool $useCert 是否需要证书，默认不需要
     * @param int $second   url执行超时时间，默认30s
     * @throws WxPayException
     */
    private  static function postXmlCurl($xml, $url, $useCert = false, $second = 30)
    {
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);

        //如果有配置代理这里就设置代理
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,TRUE);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);//严格校验
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        if($useCert == true){
            //设置证书
            //使用证书：cert 与 key 分别属于两个.pem文件
            curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');
            curl_setopt($ch,CURLOPT_SSLCERT, WX_PATH."/cert/apiclient_cert.pem");
            curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
            curl_setopt($ch,CURLOPT_SSLKEY, WX_PATH."/cert/apiclient_key.pem");
        }
        //post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        //运行curl
        $data = curl_exec($ch);
        //返回结果
        if($data){
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            throw new Exception("curl出错，错误码:$error");
        }
    }
	/*

	Get

	*/
	public function get_url($url){
		//初始化curl
		$ch = curl_init();
		//设置超时
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,FALSE);
		curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,FALSE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		//运行curl，结果以jason形式返回
		$res = curl_exec($ch);
		curl_close($ch);
		return $res;
	}


	/* ========================================== */
	/**
	 * 加密
	 * @param string $str 要处理的字符串
	 * @param string $key 加密Key，为8个字节长度
	 * @return string
	 */
	function do_mencrypt($input, $key)
	{
		//$input = str_replace(""n", "", $input);
		//$input = str_replace(""t", "", $input);
		//$input = str_replace(""r", "", $input);
		$key = substr(md5($key), 0, 24);
		$td = mcrypt_module_open('tripledes', '', 'ecb', '');
		$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
		mcrypt_generic_init($td, $key, $iv);
		$encrypted_data = mcrypt_generic($td, $input);
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
		return trim(chop(base64_encode($encrypted_data)));
	}

	//$input - stuff to decrypt
	//$key - the secret key to use

	function do_mdecrypt($input, $key)
	{
		//$input = str_replace(""n", "", $input);
		//$input = str_replace(""t", "", $input);
		//$input = str_replace(""r", "", $input);
		$input = trim(chop(base64_decode($input)));
		$td = mcrypt_module_open('tripledes', '', 'ecb', '');
		$key = substr(md5($key), 0, 24);
		$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
		mcrypt_generic_init($td, $key, $iv);
		$decrypted_data = mdecrypt_generic($td, $input);
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
		return trim(chop($decrypted_data));
	}










	/*

	写日志

	*/
	public function logs($file,$word){
		$fp = fopen($file,"a");
		flock($fp, LOCK_EX) ;
		fwrite($fp,"".$word."\r\n");
		flock($fp, LOCK_UN);
		fclose($fp);
	}
	/*

	写日志

	*/
	public function log_txt($file,$word){
		$fp = fopen($file,"a");
		flock($fp, LOCK_EX) ;
		fwrite($fp,"<?php exit;?>日期:".strftime("%Y-%m-%d-%H:%M:%S",time())."-> ".$word."\r\n");
		flock($fp, LOCK_UN);
		fclose($fp);
	}

    public function post_url_apitest($url){
        $cfg = $this->lod_cfg('_cfg');
        $data= array();
        $data["mch_id"] = $cfg['pay']['payid'];
        $data["nonce_str"] = $this->createNonceStr();
        $data["sign"] =$this->getSign($data,$cfg['pay']['key']);
        $xml = $this->arrayToXml($data);
        $data_r= $this->xmlToArray($this->postXmlCurl($xml,$url,true,30));
        return $data_r;
    }
}


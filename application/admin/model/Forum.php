<?php

namespace app\admin\model;

use think\Model;


class Forum extends Model
{

    

    

    // 表名
    protected $name = 'forum';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'Nocomment_text',"Status_text"
    ];


    public function getStatusList()
    {
        return ['0' => __('Status 0'), '1' => __('Status 1')];
    }

    public function getNocommentList()
    {
        return ['0' => __('Nocomment 0'), '1' => __('Nocomment 1')];
    }



    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getNocommentTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['nocomment']) ? $data['nocomment'] : '');
        $list = $this->getNocommentList();
        return isset($list[$value]) ? $list[$value] : '';
    }



    public function member()
    {
        return $this->belongsTo('member', 'member_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

    public function forums()
    {
        return $this->belongsTo('forum', 'forum_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }




}

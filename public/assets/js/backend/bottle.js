define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'bottle/index' + location.search,
                    add_url: 'bottle/add',
                    edit_url: 'bottle/edit',
                    del_url: 'bottle/del',
                    multi_url: 'bottle/multi',
                    table: 'bottle',
                }
            });

            var table = $("#table");

            top.window.Backend.api.sidebar({
                'auth/admin':0
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'member.name', title: __('Member_id')},
                        {field: 'bid', title: __('Bid')},
                        {field: 'to_uid', title: __('To_uid')},
                        {field: 'fromtime', title: __('Fromtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'totime', title: __('Totime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'status', title: __('Status'),searchList: {"0":__('Status 0'),"1":__('Status 1')}, formatter: Table.api.formatter.normal},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
<?php

namespace app\api\controller;

use app\common\controller\Api;
use think\Db;


/**
 * 首页接口
 */
class bottle extends Api
{





    /**
     * 验证用户是否授权
     * @param $user_id
     * @return bool|mixed
     */
    public function wx_auth($user_id){

        $modeluser = Db::name('Member') ;
        $uid = $modeluser->field('id')->where('id',$user_id)->find()['id'];

        if( $uid ){
            return $uid;
        }else{
            return false;
        }

    }



    /**
     * 微信使用。get构造链接url
     * @param $url
     * @return mixed
     */

    public function vget($url)
    {
        $info = curl_init();
        curl_setopt($info, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($info, CURLOPT_HEADER, 0);
        curl_setopt($info, CURLOPT_NOBODY, 0);
        curl_setopt($info, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($info, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($info, CURLOPT_URL, $url);
        $output = curl_exec($info);
        curl_close($info);
        return $output;

    }
}

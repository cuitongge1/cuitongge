<?php

namespace app\admin\model;

use think\Model;


class Bottle extends Model
{

    

    

    // 表名
    protected $name = 'bottle';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'fromtime_text',
        'totime_text',"Status_text"
    ];



    public function getStatusList()
    {
        return ['0' => __('Status 0'), '1' => __('Status 1')];
    }


    public function getFromtimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['fromtime']) ? $data['fromtime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getTotimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['totime']) ? $data['totime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setFromtimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setTotimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function member()
    {
        return $this->belongsTo('member', 'member_id')->setEagerlyType(0);
    }

}

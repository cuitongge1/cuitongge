<?php

namespace app\api\controller;

use app\common\controller\Api;
use think\exception\WXBizDataCrypt;
use think\Db;


/**
 * 首页接口
 */
class Index extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];





    /*
          *登录（调用wx.login获取）
          * @param $code string
          * @param $rawData string
          * @param $signatrue string
          * @param $encryptedData string
          * @param $iv string
          * @return $code 成功码
          * @return $session3rd  第三方3rd_session
          * @return $data  用户数据
      */
    public function login()
    {
        //开发者使用登陆凭证 code 获取 session_key 和 openid
        $APPID = '';//自己配置
        $AppSecret = '';//自己配置


        $code = $this->request->request("code");
        $url = "https://api.weixin.qq.com/sns/jscode2session?appid=" . $APPID . "&secret=" . $AppSecret . "&js_code=" . $code . "&grant_type=authorization_code";
        $arr = $this->vget($url);  // 一个使用curl实现的get方法请求
        $arr = json_decode($arr, true);
        $openid = $arr['openid'];
        $session_key = $arr['session_key'];
        // 数据签名校验
        $signature = $this->request->request("signature");
        $rawData = $this->request->request("rawData");

        $signature2 = sha1($rawData . $session_key);
        if ($signature != $signature2) {
            $this->error(__('数据签名验证失败！'));
        }


        $encryptedData = $this->request->request('encryptedData');
        $iv =  $this->request->request('iv');


        if(empty($signature) || empty($encryptedData) || empty($iv)){

            $this->error(__('传递信息不全！'));
        }

        $pc = new WXBizDataCrypt($APPID, $session_key);
        $data = $this->request->request("data");
        $errCode = $pc->decryptData($encryptedData, $iv, $data);  //其中$data包含用户的所有数据
        $data = json_decode($data,true);
        if ($errCode == 0) {

            $userInfo = [
                'openid'    => $data['openId'],
                'nickName'   => $data['nickName'],
                'sex'   => $data['sex'],
                'create_time'=> date('Y-m-d H:i:s'),
                'update_time'=> date('Y-m-d H:i:s'),
            ];

            $modeluser = model('Member');
            $uid = $modeluser->field('id')->where('openid',$data['openId'])->find()['id'];
            //判断用户是否已经授权登录（没有则注册）
            if(empty($uid)) {
                $uid = $modeluser->isUpdate(true)->save();
            }

            if($uid){

                $this->success('授权成功',$uid);
            }else{
                $this->error(__('授权失败'));
            }

            die;//打印解密所得的用户信息
        } else {
            $this->error(__('解密数据失败'));
        }
    }





    /**
     * 首页
     *
     */
    public function index()
    {

        //验证是否登录
        $result = $this->wx_auth($this->request->request("user_id"));

        if($result){
            $this->success('授权成功',$result);
        }else{
            $url = input('server.REQUEST_SCHEME') . '://' . input('server.SERVER_NAME').'/public/api/index/login';
            $result['url'] =  $url;
            $this->error('请重新授权',$result);
        }


    }






    /**
     * 微信使用。get构造链接url
     * @param $url
     * @return mixed
     */

    public function vget($url)
    {
        $info = curl_init();
        curl_setopt($info, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($info, CURLOPT_HEADER, 0);
        curl_setopt($info, CURLOPT_NOBODY, 0);
        curl_setopt($info, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($info, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($info, CURLOPT_URL, $url);
        $output = curl_exec($info);
        curl_close($info);
        return $output;

    }
}

<?php

return [
    'Id'         => 'id',
    'Title'      => '文字标题',
    'Images'     => '缩略图',
    'Content'    => '信息内容',
    'Createtime' => '发布时间',
    'Updatetime' => '更新时间',
    'Member_id'  => '发布者id'
];

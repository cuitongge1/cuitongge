<?php

return [
    'Bid'       => '漂流瓶id',
    'Ctime'     => '回复时间',
    'Content'   => '回复内容',
    'Member_id' => '发布者',
    'Chat_id'   => '回复文章ID'
];

<?php

namespace app\admin\model;

use think\Model;


class Member extends Model
{

    

    

    // 表名
    protected $name = 'member';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        "nospeak_text","sex_text","nopublish_text","status_text"
    ];


    public function getNospeakList()
    {
        return ['0' => __('Nospeak 0'), '1' => __('Nospeak 1')];
    }
    public function getSexList()
    {
        return ['0' => __('Sex 0'), '1' => __('Sex 1')];
    }
    public function getNopublishList()
    {
        return ['0' => __('Nopublish 0'), '1' => __('Nopublish 1')];
    }
    public function getStatusList()
    {
        return ['0' => __('Status 0'), '1' => __('Status 1')];
    }


    public function getNospeakTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['nospeak']) ? $data['nospeak'] : '');
        $list = $this->getNospeakList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getNopublishTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['nopublish']) ? $data['nopublish'] : '');
        $list = $this->getNopublishList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getSexTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['sex']) ? $data['sex'] : '');
        $list = $this->getSexList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getstatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    







}
